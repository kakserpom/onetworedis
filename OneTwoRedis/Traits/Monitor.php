<?php
namespace OneTwoRedis\Traits;
use OneTwoRedis\MonitorStatItem;

trait Monitor
{

    /**
     * Base ncurses init
     */
    protected function ncursesPreInit() {
        if (!function_exists('ncurses_init')) {
            print 'pecl-ncurses has to be installed.' . PHP_EOL;
            exit(1);
        }
        function ncurses_addstr_colored($text, $color)
        {
            ncurses_has_colors() && ncurses_color_set($color);
            ncurses_addstr($text);
            ncurses_has_colors() && ncurses_color_set(0);
        }
    }

    /**
     * ncurses initialization
     */
    protected function ncursesInit() {
        ncurses_init();
        ncurses_halfdelay(1);
        ncurses_echo();

        if (ncurses_has_colors()) {
            ncurses_start_color();
            ncurses_assume_default_colors(NCURSES_COLOR_WHITE, NCURSES_COLOR_BLUE);

            // Dots
            ncurses_init_pair(1, NCURSES_COLOR_WHITE, NCURSES_COLOR_BLUE);

            // Titles
            ncurses_init_pair(2, NCURSES_COLOR_YELLOW, NCURSES_COLOR_BLUE);

            // Path
            ncurses_init_pair(3, NCURSES_COLOR_CYAN, NCURSES_COLOR_BLUE);

            // Press 'q' to exit
            ncurses_init_pair(4, NCURSES_COLOR_YELLOW, NCURSES_COLOR_BLACK);

            // BORDER
            ncurses_init_pair(5, NCURSES_COLOR_WHITE, NCURSES_COLOR_BLUE);
        }
    }

    protected function getMonitorData() {
        $this->redis->pipeline();
        $this->redis->llen($this->queueKey);            // 0
        $this->redis->get($this->queueKeyConsumedCnt);  // 1
        $this->redis->get($this->queueKeyTotalCnt);     // 2
        $this->redis->llen($this->queueKeyErrors);      // 3
        $this->redis->lrange($this->queueKey, -20, -1); // 4
        $this->redis->lrange($this->queueKey, 0, 19);   // 5
        return $this->redis->exec();
    }
    /**
     *
     */
    protected function monitorMode() {
        $this->ncursesPreInit();
        $this->ncursesInit();
        $screen = ncurses_newwin(0, 0, 0, 0);
        ncurses_getmaxyx($screen, $rows, $cols);

        $tries = 0;
        start:
        ncurses_clear();
        try {
            $repeat = false;
            $this->connect();
            $tries = 0;
            $this->setProcTitle('MONITOR');
            $lastDataRefresh = 0;
            $dataRefreshInterval = 0.2;

            pcntl_signal(SIGWINCH, function ()  use (&$rows, &$cols, &$screen ) {
                ncurses_clear();
                ncurses_refresh();
                ncurses_end();
                $this->ncursesInit();
                ncurses_clear();
                ncurses_refresh();
                $screen = ncurses_newwin(0, 0, 0, 0);
                ncurses_getmaxyx($screen, $rows, $cols);
            });


            $frame = 0;
            $consumedStats = new MonitorStatItem;
            $totalStats = new MonitorStatItem;
            for (; ;) {
                if ($lastDataRefresh < microtime(true) - $dataRefreshInterval) {
                    $lastDataRefresh = microtime(true);

                    $ret = $this->getMonitorData();
                    if (!isset($ret[0])) {
                        time_nanosleep(0, 0.2 * 1e9);
                        goto start;
                    }

                    $backlogSize    = $ret[0];
                    $consumed       = (int)$ret[1];
                    $total          = (int)$ret[2];
                    $numOfErrors    = $ret[3];
                    $backlogNewest  = $ret[4];
                    $backlogOldest  = $ret[5];

                    $consumedStats->push($consumed);
                    $consumedRate = $consumedStats->calc();

                    $totalStats->push($total);
                    $totalRate = $totalStats->calc();


                }


                $row = 2;
                ncurses_move($row, 5);
                ncurses_has_colors() && ncurses_color_set(3);
                ncurses_addstr(implode(' ', $_SERVER['argv']));
                ncurses_has_colors() && ncurses_color_set(0);

                $row += 1;
                ncurses_move($row, 5);
                ncurses_clrtoeol();
                ncurses_has_colors() && ncurses_color_set(1);
                ncurses_addstr(str_repeat('.', $frame % 20));
                ncurses_has_colors() && ncurses_color_set(0);

                static $totalMessagesAlign = 35;


                $row += 2;
                ncurses_move($row, 5);
                ncurses_clrtoeol();
                ncurses_has_colors() && ncurses_color_set(2);
                ncurses_addstr('CURRENT QUEUE SIZE:');
                ncurses_move($row, $totalMessagesAlign);
                ncurses_addstr('TOTAL MESSAGES DISPATCHED:');
                ++$row;
                ncurses_has_colors() && ncurses_color_set(0);

                ncurses_move($row, 5);
                ncurses_clrtoeol();
                ncurses_has_colors() && ncurses_color_set(1);
                ncurses_addstr(number_format($backlogSize));
                ncurses_move($row, $totalMessagesAlign);
                ncurses_addstr(number_format($consumed) . PHP_EOL);
                ++$row;

                ncurses_move($row, 5);
                ncurses_clrtoeol();
                ncurses_addstr(number_format($totalRate) . ' m/s inbound');
                ncurses_move($row, $totalMessagesAlign);
                ncurses_addstr(number_format($consumedRate) . ' m/s outbound');
                ncurses_has_colors() && ncurses_color_set(0);

                $row -= 2;
                ncurses_move($row, 70);
                ncurses_clrtoeol();
                ncurses_has_colors() && ncurses_color_set(2);
                ncurses_addstr('ERRORS:');
                ncurses_has_colors() && ncurses_color_set(1);
                ++$row;
                ncurses_move($row, 70);
                ncurses_addstr(number_format($numOfErrors));
                ++$row;
                ncurses_move($row, 70);
                if ($numOfErrors > 0) {
                    ncurses_addstr('(use --getErrors to retrieve)');
                }
                ncurses_has_colors() && ncurses_color_set(0);


                /*++$row;
                ncurses_move($row, 70);
                ncurses_clrtoeol();
                ncurses_addstr_colored('REDIS MEM. USAGE: ', 2);
                ncurses_addstr_colored($redisMemory['used_memory_human'], 1);*/


                $row += 2; // Margin

                $col = 5;
                ncurses_move($row, 5);
                ncurses_addstr_colored('Newest messages:', 2);

                $row += 2;
                $maxRows = $rows - $row + 8;

                $i = 0;
                foreach ($backlogNewest as $i => $msg) {
                    ncurses_move($row + $i, $col + 5);
                    ncurses_clrtoeol();
                    ncurses_addstr(self::truncateString($msg, 20));
                    if ($row + $i > $maxRows) {
                        break;
                    }
                }
                $backlogNewestDrawn = $i;

                $row -= 2;
                $col += 50;
                ncurses_move($row, $col);
                $row += 2;
                ncurses_addstr_colored('Oldest messages:', 2);
                foreach ($backlogOldest as $i => $msg) {
                    ncurses_move($row + $i, $col + 5);
                    ncurses_addstr(self::truncateString($msg, 20));
                    if ($row + $i > $maxRows) {
                        break;
                    }
                }
                $backlogOldestDrawn = $i;

                $row = $row + max($backlogNewestDrawn, $backlogOldestDrawn);

                ++$row;

                ncurses_move(3, $cols - 24);
                ncurses_clrtoeol();
                ncurses_addstr_colored("Window size {$cols}x{$rows}", 1);

                ncurses_move(2, $cols - 25);
                ncurses_addstr_colored(" Press 'q' to exit: ", 4);
                ncurses_move(2, $cols - 5);
                ncurses_addstr('_');
                ncurses_move(2, $cols - 5);

                ncurses_has_colors() && ncurses_color_set(5);
                /*ncurses_border(
                    0, 0, 0, 0,
                    0, 0, 0, 0
                );*/
                ncurses_has_colors() && ncurses_color_set(0);
                ncurses_refresh();

                /*$messagesBox = ncurses_newwin(6, 80, 7, 10);
                ncurses_mvwaddstr($messagesBox, 0, 0, 'WAITING IN BACKLOG');
                ncurses_wborder($messagesBox,
                    ord('|'), ord('-'), ord('|'), ord('-'),
                    ord('+'), ord('+'), ord('+'), ord('+')
                );
                ncurses_wrefresh($messagesBox);*/

                $pressed = ncurses_getch();
                if ($pressed === 27 || $pressed === 113) { // ESCAPE or 'q'
                    break;
                }
                pcntl_signal_dispatch();
                ++$frame;
            }
        } catch (\Throwable $exception) {
            if ($tries++ > 0) {
                time_nanosleep(0, 0.2 * 1e9);
            }
            $repeat = true;
            goto start;
        } finally {
            if (isset($exception)) {
                ncurses_clear();
                ncurses_addstr('Connecting to Redis at ' . $this->redisHost . ':' . $this->redisPort
                    . str_repeat('.', $tries % 4)
                    . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL
                    . (string) $exception
                );
                ncurses_move(2, $cols - 25);
                ncurses_addstr("Press 'q' to exit: ");
                ncurses_refresh();
                $pressed = ncurses_getch();
                if ($pressed === 27 || $pressed === 113) { // ESCAPE or 'q'
                    ncurses_end();
                    exit;
                }
                unset($exception);
            }
            if (!$repeat) {
                if (isset($screen)) {
                    ncurses_delwin($screen);
                    $screen = null;
                }
                ncurses_clear();
                ncurses_end();
            }
        }
    }

    /**
     * @param string $str
     * @param integer $maxLen
     * @return string
     */
    protected static function truncateString($str, $maxLen) {
        $len = mb_strlen($str);
        if ($len < $maxLen) {
            return $str;
        }
        return mb_substr($str, 0, $maxLen - 3) . '...';
    }
}
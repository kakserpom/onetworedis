<?php
namespace OneTwoRedis\Traits;
trait CliInputHandler
{
    protected function connect() {
        $this->redis = new \Redis();
        if (!@$this->redis->connect($this->redisHost, $this->redisPort, 1)) {
            throw new \RedisException;
        }
        $this->redis->setOption(\Redis::OPT_READ_TIMEOUT, 2);
        $this->redLock = new \RedLock([[$this->redisHost, $this->redisPort, 0.01]]);
    }
    protected function handleCliInput() {
        $opts = getopt('', [
            'usage',
            'getErrors',
            'cuppa-coffee',
            'no-pipelining',
            'consume-only',
            'monitor'
        ]);
        $this->queueKey = $this->queue;
        $this->queueKeyErrors = $this->queue . ':errors';
        $this->queueKeyTotalCnt = $this->queue . ':cnt-total';
        $this->queueKeyConsumedCnt = $this->queue . ':cnt-consumed';
        if (isset($opts['usage'])) {
            print 'USAGE: ' . $_SERVER['argv'][0] . PHP_EOL
                . "\t\t--cuppa-coffee (optional, disables sleep() in the generator)" . PHP_EOL
                . "\t\t--no-pipelining (optional, disables pipelining in the --cuppa-coffee mode)" . PHP_EOL
                . "\t\t--consume-only (optional, prevents this instance from becoming the generator)" . PHP_EOL
                . "Tools:" . PHP_EOL
                . "\t\t--getErrors (retrieve all errors)" . PHP_EOL
                . "\t\t--monitor (CLI monitoring mode)" . PHP_EOL
                . PHP_EOL;
        }
        elseif (isset($opts['getErrors'])) {
            $this->getErrors();
        }
        elseif (isset($opts['monitor'])) {
            $this->monitorMode();
        }
        else {
            if (isset($opts['cuppa-coffee'])) {
                $this->generatorInterval = 0;
                if (!isset($opts['no-pipelining'])) {
                    $this->usePipelining = true;
                }
            }
            if (isset($opts['consume-only'])) {
                $this->consumeOnly = true;
            }
            $this->run();
        }
    }

    /**
     * Change the process title
     * @param string $title
     */
    protected function setProcTitle($title = null) {
        $this->setTermTitle($title);
        if (function_exists('cli_set_process_title')) {
            cli_set_process_title(implode(' ', $_SERVER['argv']) . ($title !== null ? ' (' . $title . ')' : ''));
        }
    }

    /**
     * Change the terminal title
     * @param $title
     */
    protected function setTermTitle($title) {
        print "\033]0;$title\007";
    }
}
<?php
namespace OneTwoRedis;


class Cli {
    use Traits\CliInputHandler;
    use Traits\Monitor;

    /**
     * Redis host
     * @var string
     */
    protected $redisHost = '127.0.0.1';

    /**
     * Redis port
     * @var int
     */
    protected $redisPort = 6379;

    /**
     * Interval between new messages in milliseconds
     * @var int
     */
    protected $generatorInterval = 500;

    /**
     * Redis lock name
     * @var string
     */
    protected $generatorLockName = 'generator-lock';

    /**
     * Redis lock time (milliseconds)
     * @var string
     */
    protected $generatorLockTime = 5000;

    /**
     * Redis lock time (milliseconds)
     * @var string
     */
    protected $generatorLockChangeTimeout = 7000;


    /*
     * Queue
     *
     * @var string
     */
    protected $queue = 'queue';

    /**
     * Redis key for the queue (list)
     * @var string
     */
    protected $queueKey;

    /**
     * Redis key for errors (list)
     * @var string
     */
    protected $queueKeyErrors;

    /**
     * Redis key for counter of consumed messages (integer)
     * @var string
     */
    protected $queueKeyConsumedCnt;

    /**
     * Redis key for counter of messages (integer)
     * @var string
     */
    protected $queueKeyTotalCnt;

    /**
     * @var \RedLock
     */
    protected $redLock;

    /**
     * @var \Redis
     */
    protected $redis;

    /**
     * Use Redis pipelining?
     * @var bool
     */
    protected $usePipelining = false;

    /**
     * Use Redis batchpolling?
     * @var bool
     */
    protected $useBatchPolling = true;

    /**
     * @var int
     */
    protected $pollingBatchSize = 2000;

    /**
     * @var bool
     */
    protected $consumeOnly = false;

    /**
     *
     * @var int
     */
    protected $batchSize = 2000;

    /**
     * @var array
     */
    protected $erroneousMessagesBatch = [];
    
    /**
     * Cli constructor.
     */
    public function __construct() {
        set_error_handler(function ($errno, $errstr, $errfile, $errline ,array $errcontex) {
            if (strpos($errstr, 'Redis::') === 0) {
                throw new \RedisException($errstr, $errno);
            } else {
                throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        });
        $this->handleCliInput();
    }


    /**
     * @param $message
     */
    protected function processMessage($message) {
        if (mt_rand(0, 100) <= 5) {
            $this->reportError($message);
        } else {
            //echo "OK: $message" .  PHP_EOL;
        }
    }

    /**
     * Flushes errorneus messages
     */
    protected function flushErroneousMessages() {
        $this->redis->rawCommand('RPUSH', $this->queueKeyErrors, ...$this->erroneousMessagesBatch);
        $this->erroneousMessagesBatch = [];
    }

    /**
     * @param $message
     */
    protected function reportError($message) {
        if ($this->useBatchPolling) {
            $this->erroneousMessagesBatch[] = $message;
            return;
        }
        //echo "ERROR DETECTED: $message" . PHP_EOL;
        $tries = 0;
        start:
        try {
            $this->redis->rPush($this->queueKeyErrors, $message);
        } catch  (\RedisException $e) {
            if ($tries++ > 0) {
                time_nanosleep(0, 0.2 * 1e9);
            }
            $this->connect();
            goto start;
        }
    }

    /**
     *
     */
    protected function generateMessage() {
        $message = base64_encode(openssl_random_pseudo_bytes(10));
        //print "Pushing $message into the queue" . PHP_EOL;
        return $message;
    }

    /**
     * Run the application
     */
    protected function run() {
        $tries = 0;
        start:
        try {
            $this->connect();
            $tries = 0;
            $generatorMode = false;
            while (true) {
                if ($this->consumeOnly) {
                    $lock = false;
                } elseif (!$generatorMode && $this->redis->get($this->generatorLockName . '-until') > time()) {
                    $lock = false;
                } else {
                    $lock = $this->redLock->lock($this->generatorLockName, $this->generatorLockTime);
                }
                if ($lock) {
                    if (!$generatorMode) {
                        echo "GENERATOR LOCK ACQUIRED" . PHP_EOL . PHP_EOL;
                        $generatorMode = true;
                        $this->setProcTitle('GENERATOR');
                    }

                    $until = time() + ceil($lock['validity'] / 1000) + 0.5;

                    // Prevent other instance from trying to acquire the lock too early
                    $this->redis->set($this->generatorLockName . '-until',
                        time() + $this->generatorLockChangeTimeout / 1000,
                        ['PX' => $this->generatorLockChangeTimeout]
                    );

                    if ($this->usePipelining) {
                        $this->redis->pipeline();
                        $batchSize = 0;
                    }
                    while ($until > time()) {
                        $message = $this->generateMessage();
                        $this->redis->rpush($this->queueKey, $message);
                        if ($this->usePipelining) {
                            ++$batchSize;
                            if ($batchSize >= $this->batchSize) {
                                $this->redis->incrby($this->queueKeyTotalCnt, $batchSize);
                                $this->redis->exec();
                                $this->redis->pipeline();
                                $batchSize = 0;
                            }
                        }
                        if ($this->generatorInterval !== 0) {
                            usleep($this->generatorInterval * 1e3);
                        }
                    }
                    if ($this->usePipelining) {
                        if ($batchSize > 0) {
                            $this->redis->incrby($this->queueKeyTotalCnt, $batchSize);
                            $this->redis->exec();
                            $batchSize = 0;
                        } else {
                            $this->redis->discard();
                        }
                    }
                } else {
                    $this->setProcTitle('CONSUMER');
                    $generatorMode = false;
                    echo 'POLLING' . PHP_EOL;
                    $until = (int)$this->redis->get($this->generatorLockName . '-until')
                        ?: time() + ceil($this->generatorLockTime / 1000 / 2);
                    $i = 0;
                    while ($until > time()) {
                        if ($i > 0 && $this->useBatchPolling) {
                            $this->redis->multi();
                            $this->redis->lrange($this->queueKey, 0, $this->pollingBatchSize - 1);
                            $this->redis->ltrim($this->queueKey, $this->pollingBatchSize - 1, -1);
                            $this->redis->incr($this->queueKeyTotalCnt);
                            $exec = $this->redis->exec();
                            if ($exec === false) {
                                print 'Redis error: ' . $this->redis->getLastError() . PHP_EOL;
                                time_nanosleep(0, 0.2 * 1e9);
                            }
                            $messages = $exec[0];
                            $i = 0;
                            foreach ($messages as $message) {
                                try {
                                    $this->processMessage($message);
                                } catch (\Exception $e) {

                                } finally {
                                    ++$i;
                                }
                            }
                            $this->redis->pipeline();
                            $this->redis->incrBy($this->queueKeyConsumedCnt, $i);
                            if ($this->erroneousMessagesBatch) {
                                $this->flushErroneousMessages();
                            }
                            $this->redis->exec();
                            if ($i > 0) {
                                continue;
                            }
                        }
                        $reply = $this->redis->blPop($this->queueKey, 2);
                        if ($reply === false) {
                            print 'Redis error: ' . $this->redis->getLastError() . PHP_EOL;
                            time_nanosleep(0, 0.2 * 1e9);
                        }
                        elseif ($reply) {
                            $i = 1;
                            try {
                                $this->processMessage($reply[1]);
                            }
                            catch (\Exception $e) {
                            }
                            finally {
                                $this->redis->pipeline();
                                $this->redis->incr($this->queueKeyConsumedCnt);
                                if ($this->erroneousMessagesBatch) {
                                    $this->flushErroneousMessages();
                                }
                                $this->redis->exec();
                            }
                        } else {
                            $i = 0;
                        }
                    }
                }
            }
        } catch (\RedisException $e) {
            if ($tries++ > 0) {
                time_nanosleep(0, 0.2 * 1e9);
            }
            goto start;
        }
    }


    /**
     * Fetch errors from Redis and remove them
     */
    protected function getErrors() {
        start:
        $tries = 0;
        try {
            $this->connect();
            $tries = 0;
            for (; ;) {
                $errors = $this->redis->lrange($this->queueKeyErrors, 0, 1000);
                if (!$errors) {
                    break;
                }
                print implode(PHP_EOL, $errors) . PHP_EOL;
                $this->redis->ltrim($this->queueKeyErrors, count($errors), -1);
            }
        } catch (\RedisException $e) {
            if ($tries++ > 0) {
                time_nanosleep(0, 0.2 * 1e9);
            }
            goto start;
        }
    }
}

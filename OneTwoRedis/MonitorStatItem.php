<?php
namespace OneTwoRedis;


class MonitorStatItem {

    /**
     * Stat items
     * @var array
     */
    protected $items = [];
    
    /**
     * @var int
     */
    protected $threshold;

    /**
     * MonitorStatItem constructor.
     * @param int $threshold
     */
    public function __construct($threshold = 10) {
        $this->threshold = $threshold;
    }

    /**
     * @param $value
     */
    public function push($value) {
        $this->items[] = [microtime(true), $value];
    }

    /**
     * @return float|string
     */
    public function calc() {
        if (count($this->items) < 2) {
            return 0;
        }
        $this->items = array_slice($this->items, -$this->threshold);
        $first = $this->items[0];
        $last = end($this->items);
        $timeDiff = $last[0] - $first[0];
        $valueDiff = $last[1] - $first[1];
        return 1 / $timeDiff * $valueDiff;
    }
}

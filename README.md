```
USAGE: ./bin/OneTwoRedis
		--cuppa-coffee (optional, disables sleep() in the generator)
		--no-pipelining (optional, disables pipelining in the --cuppa-coffee mode)
		--consume-only (optional, prevents this instance from becoming the generator)
Tools:
		--getErrors (retrieve all errors)
		--monitor (CLI monitoring mode)
```
-----------------------------------------------------------------------------------------
[--monitor demo (gif)](http://www.giphy.com/gifs/3o7bu43bAPHj2CLdRe)